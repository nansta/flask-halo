import pytest
import requests

import random
import string

def get_random_username():
    return "".join([random.choice(string.ascii_letters + string.digits) for n in range(32)])

BASE_URL = "http://127.0.0.1:5050/api/v1"
USERNAME = get_random_username()

@pytest.fixture
def jwt_token():
    """This fixture returns a user jwt token."""
    response = requests.post(
        "{}/sign_in".format(BASE_URL),
        json = {
            "username": USERNAME,
            "password": "ilovehalo",
        },
        headers = {
            "Content-Type": "application/json"
        }
    )

    assert response.status_code == requests.codes.ok

    return response.json().get("access_token")

def test_basic():
    """This tests that we can hit the flask server."""
    r = requests.get(
        "{}/".format(BASE_URL))

    assert r.status_code == 404

def test_sign_up_missing_username():
    """This tests that a user can't sign up without a username."""
    response = requests.put(
        "{}/sign_up".format(BASE_URL),
        json = {},
        headers = {
            "Content-Type": "application/json"
        }
    )

    assert response.status_code == requests.codes.ok

    # Test that a user can register
    message = response.json()
    assert message.get("message") == "Missing username parameter"

def test_sign_up_missing_password():
    """This tests that a user can't sign up without a password."""
    response = requests.put(
        "{}/sign_up".format(BASE_URL),
        json = {
            "username": "user_does_not_exit",
        },
        headers = {
            "Content-Type": "application/json"
        }
    )

    assert response.status_code == requests.codes.ok

    # Test that a user can register
    message = response.json()
    assert message.get("message") == "Missing password parameter"


def test_sign_up_existing_username():
    """This tests that a user can't sign up using an existing username."""
    response = requests.put(
        "{}/sign_up".format(BASE_URL),
        json = {
            "username": USERNAME + 'X',
            "password": "ilovehalo",
        },
        headers = {
            "Content-Type": "application/json"
        }
    )

    assert response.status_code == requests.codes.created

    # Test that a user can register
    message = response.json()
    assert message.get("id") is not None
    assert message.get("username") is not None
    assert message.get("access_token") is not None

    response = requests.put(
        "{}/sign_up".format(BASE_URL),
        json = {
            "username": USERNAME + 'X',
            "password": "ilovehalo",
        },
        headers = {
            "Content-Type": "application/json"
        }
    )

    assert response.status_code == requests.codes.ok

    # Test that a user can register
    message = response.json()
    assert message.get("message") == "Username already exists"

def test_auth():
    """This tests that a user can sign up and sign in."""
    response = requests.put(
        "{}/sign_up".format(BASE_URL),
        json = {
            "username": USERNAME,
            "password": "ilovehalo",
        },
        headers = {
            "Content-Type": "application/json"
        }
    )

    assert response.status_code == requests.codes.created

    # Test that a user can register
    message = response.json()
    assert message.get("id") is not None
    assert message.get("username") is not None
    assert message.get("access_token") is not None

    response = requests.post(
        "{}/sign_in".format(BASE_URL),
        json = {
            "username": USERNAME,
            "password": "ilovehalo",
        },
        headers = {
            "Content-Type": "application/json"
        }
    )

    assert response.status_code == requests.codes.ok

    # Test that a user can sign in
    message = response.json()
    assert message.get("id") is not None
    assert message.get("username") is not None
    assert message.get("access_token") is not None

def test_get_nonexisting_key(jwt_token):
    """This tests that a user cannot get an unset key."""
    response = requests.post(
        "{}/data/get".format(BASE_URL),
        json = {
            "key": "key_does_not_exist",
        },
        headers = {
            "Content-Type": "application/json",
            "Authorization": "Bearer {}".format(jwt_token),
        }
    )

    assert response.status_code == requests.codes.not_found

    # Test that a user can't get an unset key
    message = response.json()
    assert message.get("message") == "Item not found: Could not find the key key_does_not_exist"

def test_set_get(jwt_token):
    """This tests that a user can set and get a key value pair."""
    response = requests.put(
        "{}/data/set".format(BASE_URL),
        json = {
            "key": "hello",
            "value": "world!",
        },
        headers = {
            "Content-Type": "application/json",
            "Authorization": "Bearer {}".format(jwt_token),
        }
    )

    assert response.status_code == requests.codes.ok

    # Test that a user can set a key and value
    message = response.json()
    assert message.get("id") is not None
    assert message.get("key") == "hello"
    assert message.get("value") == "world!"

    response = requests.post(
        "{}/data/get".format(BASE_URL),
        json = {
            "key": "hello",
        },
        headers = {
            "Content-Type": "application/json",
            "Authorization": "Bearer {}".format(jwt_token),
        }
    )

    assert response.status_code == requests.codes.ok

    # Test that a user can get a value from a key
    message = response.json()
    assert message.get("value") is not None
    assert message.get("value") == "world!"
