# flask-halo

### Live Application
You can use the production deployed application at:
http://ec2-3-81-116-20.compute-1.amazonaws.com:5050

#### Examples
```
$ curl -d '{"username":"test", "password":"test"}' -H "Content-Type: application/json" -X POST http://ec2-3-81-116-20.compute-1.amazonaws.com:5050/api/v1/sign_in
```

### Requirements
- Ubuntu 16.04.6 LTS
- Python 3.6.9
- Pip 19.3.1
- Virtualenv 15.1.0

Install python
```
$ sudo apt-get install python
```

Install pip
```
$ sudo apt-get install python-pip
```

Install virtualenv
```
$ sudo apt-get install virtualenv
```

### Installation
Setup a virtual environment
```
$ cd flask-halo/
$ virtualenv env/ -p python3
```
Activate virtual env
```
$ source env/bin/activate
```
Install python packages
```
$ pip install -r requirements.txt
```
Copy a new config file
```
$ cp halo/config.py.sample halo/config.py
```
Run migration upgrade
```
$ python -m flask db upgrade -d halo/migrations/
```
Run migration
```
$ python -m flask db migrate -d halo/migrations
```

### Run
Run the application
```
$ python -m flask run --host 0.0.0.0 --port 5050
```

### Run tests
Run the application
```
$ cd flask-halo
$ python -m flask run --host 0.0.0.0 --port 5050
```
Run integration tests
```
$ pytest
```
