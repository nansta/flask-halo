from flask import Flask
from flask_cors import CORS
from flask_migrate import Migrate
from flask_restful import Resource, Api
from flask_jwt_extended import JWTManager

from halo.models import db

from halo.resources import user, auth, data

# Flask
app = Flask(__name__)

# Flask-Cors
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})

# Flask-Restful
api = Api(app)

# Configuration
app.config.from_object("halo.config.Config")

# Flask-JWT
jwt = JWTManager(app)

# Flask-SQLAlchemy
db.init_app(app)

# Flask-Migrate
migrate = Migrate(app, db)

api.add_resource(user.UserDataResource, '/api/v1/user/data')

api.add_resource(data.DataGetResource, '/api/v1/data/get')
api.add_resource(data.DataSetResource, '/api/v1/data/set')

api.add_resource(auth.SignUpResource, '/api/v1/sign_up')
api.add_resource(auth.SignInResource, '/api/v1/sign_in')
