from flask import make_response, jsonify

def error(message, status_code=200): 
    return make_response(jsonify({'message': message}), status_code)

def bad_request(message):
    return error("Bad Request: {}".format(message), 400)

def item_not_found(message): 
    return error("Item not found: {}".format(message), 404)