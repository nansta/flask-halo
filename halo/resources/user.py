from flask import jsonify, request
from flask_restful import Resource, marshal_with

from flask_jwt_extended import (
    create_access_token,
    jwt_required,
    get_jwt_identity
)

from werkzeug.security import safe_str_cmp

from halo.models.user import User
from halo.models.data import Data

class UserDataResource(Resource):
    @jwt_required
    @marshal_with(Data.fields, envelope='data')
    def get(self):
        identity = get_jwt_identity()
        user = User.query.filter_by(username=identity).first()

        return user.data
