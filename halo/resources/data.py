from flask import jsonify, request
from flask_restful import Resource, marshal

from flask_jwt_extended import (
    jwt_required,
    get_jwt_identity
)

from werkzeug.security import safe_str_cmp

from halo.models import db
from halo.models.user import User
from halo.models.data import Data
from .error_handeling import (
    bad_request,
    item_not_found
)

class DataGetResource(Resource):
    @jwt_required
    def post(self):
        if not request.is_json:
            return bad_request("Missing JSON in request")

        key = request.json.get('key', None)
        if not key:
            return bad_request("Missing key parameter")

        identity = get_jwt_identity()
        user = User.query.filter_by(username=identity).first()

        data = Data.query.filter_by(key=key, user_id=user.id).first()
        if not data:
            return item_not_found("Could not find the key {}".format(key))

        return marshal(data, Data.fields), 200

class DataSetResource(Resource):
    @jwt_required
    def put(self):
        if not request.is_json:
            return bad_request("Missing JSON in request")

        key = request.json.get('key', None)
        value = request.json.get('value', None)
        if not key:
            return bad_request("Missing key parameter")
        if not value:
            return bad_request("Missing value parameter")

        identity = get_jwt_identity()
        user = User.query.filter_by(username=identity).first()

        data = user.set_data(key, value)
        db.session.add(data)
        db.session.commit()

        return marshal(data, Data.fields), 200