from flask import jsonify, request
from flask_restful import Resource, marshal

from flask_jwt_extended import (
    create_access_token,
    jwt_required,
    get_jwt_identity
)

from werkzeug.security import safe_str_cmp

from .error_handeling import error

from halo.models import db
from halo.models.user import User
from .error_handeling import (
    error,
    bad_request,
)

class SignUpResource(Resource):
    def put(self):
        if not request.is_json:
            return bad_request("Missing JSON in request")

        username = request.json.get('username', None)
        password = request.json.get('password', None)
        if not username:
            return error("Missing username parameter")
        if not password:
            return error("Missing password parameter")

        user = User.query.filter_by(username=username).first()
        if user:
            return error("Username already exists")

        user = User(username=username, password=password)
        db.session.add(user)
        db.session.commit()

        return marshal(user, User.fields), 201

class SignInResource(Resource):
    def post(self):
        if not request.is_json:
            return bad_request("Missing JSON in request")

        username = request.json.get('username', None)
        password = request.json.get('password', None)
        if not username:
            return error("Missing username parameter")
        if not password:
            return error("Missing password parameter")

        user = User.query.filter_by(username=username).first()
        if not user or not user.check_password(password.encode('utf-8')):
            return error("Bad username or password")

        return marshal(user, User.fields), 200