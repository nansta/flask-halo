from halo.models import db
from flask_restful import fields
from sqlalchemy.orm import relationship

from werkzeug import generate_password_hash

class Data(db.Model):
    __tablename__ = 'data'
    __searchable__ = ['key', 'value']

    fields = {
        'id': fields.Integer,
        'key': fields.String,
        'value': fields.String
    }

    id = db.Column(
        db.Integer,
        primary_key=True,
        unique=True,
        nullable=False
    )

    key = db.Column(db.String(100))
    value = db.Column(db.String(100))

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

    def __init__(
        self,
        key,
        value,
        user_id
    ):
        self.key = key
        self.value = value
        self.user_id = user_id
