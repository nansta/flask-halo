from halo.models import db
from flask_restful import fields
from flask_jwt_extended import create_access_token

from werkzeug import generate_password_hash, check_password_hash

from .data import Data

class User(db.Model):
    __tablename__ = 'user'
    __searchable__ = ['username']

    fields = {
        'id': fields.Integer,
        'username': fields.String,
        'access_token': fields.String
    }

    id = db.Column(
        db.Integer,
        primary_key=True,
        unique=True,
        nullable=False
    )

    username = db.Column(db.String(15), unique=True)
    pwdhash = db.Column(db.String(100))
    data = db.relationship('Data', backref='user', lazy=True)

    def __init__(
        self,
        username,
        password
    ):
        self.username = username
        self.set_password(password)

    @property
    def access_token(self):
        return create_access_token(identity=self.username)

    def set_password(self, password):
        self.pwdhash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.pwdhash, password)

    def set_data(self, key, value):
        """Sets a key value pair for the current identity and returns the object"""
        data = Data.query.filter_by(key=key, user_id=self.id).first()

        # Update existing key value pair
        if data:
            data.value = value
            return data
        
        # Create new key value pair
        data = Data(key=key, value=value, user_id=self.id)
        db.session.add(data)
        db.session.commit()

        return data
